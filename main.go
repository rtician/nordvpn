package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path"
)

type ConfigFile struct {
	TargetPath string `json:"target_path"`
}

func toJSON(f interface{}) string {
	bytes, err := json.Marshal(f)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	return string(bytes)
}

func (c ConfigFile) toString() string {
	return toJSON(c)
}

func getConfigFile() ConfigFile {
	raw, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	var c ConfigFile
	json.Unmarshal(raw, &c)
	return c
}

func downloadFile(filepath string, url string) (err error) {
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Bad status: %s", resp.Status)
	}

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return err
	}

	return nil
}

func main() {
	configFile := getConfigFile()
	fmt.Println(configFile.TargetPath)
	fmt.Println(toJSON(configFile))

	// Make directory
	os.MkdirAll(configFile.TargetPath, os.ModePerm)

	// Download VPN files.
	filename := path.Join(configFile.TargetPath, "ovpn.zip")
	downloadFile(filename, "https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip")
}
